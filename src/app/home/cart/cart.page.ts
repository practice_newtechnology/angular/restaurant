import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantService } from '../restaurant-service.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  carts = [];
  filterCarts = [];
  cartAmount = this.restaurantService.cartAmount;
  showMore = true;
  canShow = true;

  constructor(
    private restaurantService: RestaurantService,
    private router: Router
  ) { }

  ngOnInit() {
    this.carts = this.restaurantService.cart;
    this.filterCarts = [...this.carts];
    this.filterCarts = this.filterCarts.slice(0, 2)
  }

  showMoreCarts() {
    this.filterCarts = [...this.carts]
    this.canShow = false;
  }


  addItem(item) {
    this.restaurantService.addCart(item);
  }

  removeItem(item) {
    if(item.count == 1) {
      this.filterCarts.splice(this.filterCarts.findIndex(x => x.id == item.id), 1);
    }
    this.restaurantService.removeCart(item);
    if(this.canShow) {
      this.filterCarts = [...this.carts];
      this.filterCarts = this.filterCarts.slice(0, 2)  
    }
  }

  goToHomePage() {
    this.router.navigateByUrl('home')
  }

}
