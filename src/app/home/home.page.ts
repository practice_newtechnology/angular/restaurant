import { Component, OnInit } from '@angular/core';
import { RestaurantService } from './restaurant-service.service';
import { map, tap } from 'rxjs/operators'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  inkaRestaurant = this.restaurantService.inkaRestaurant;
  items = this.restaurantService.items.pipe(
    map(items => {
      items.map(item => {
        item['count'] = 0;
      })
      return items;
    }),
    tap(data => console.log(data))
  );
  cartCount = this.restaurantService.cartCount;

  constructor(
    private restaurantService: RestaurantService,
    private router: Router
  ) { }

  ngOnInit(): void { }

  addItem(item) {
    this.restaurantService.addCart(item);
  }

  removeItem(item) {
    this.restaurantService.removeCart(item);
  }

  goToMyCart() {
    this.router.navigateByUrl('home/cart')
  }


}
