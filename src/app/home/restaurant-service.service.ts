import { Injectable } from '@angular/core';
import { BehaviorSubject, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  inkaRestaurant = of({
    name: 'Inka Restaurant',
    star: 5.0,
    noOfPersonRated: 200,
    timing: '09:00 AM - 06:00 PM',
    mobileNumber: '9854562142',
  })

  items = of([
    {
      id: 1,
      name: 'Guac de le costa',
      description: 'tortillas de mais, fruit de la passion, mango',
      price: 7
    },
    {
      id: 2,
      name: 'Chicharron y Cerveza',
      description: 'citron vert / corona sauce',
      price: 5
    },
    {
      id: 3,
      name: 'Sweet and Sour Shrimp',
      description: 'abcd abcd abcd abcd',
      price: 4
    },
    {
      id: 4,
      name: 'Kare-Kare Tuna',
      description: 'bcda bcda bcda',
      price: 9
    },
    {
      id: 5,
      name: 'Steam Pla-Pla"',
      description: 'dcba dbjjd kdkjkdkjd',
      price: 8
    },
    {
      id: 6,
      name: 'Inihaw na Hito',
      description: 'html djjdb hsuuejeu',
      price: 6
    },
    {
      id: 7,
      name: 'Sinigang na Lapu-Lapu',
      description: 'css jdue jdheeoi',
      price: 3
    },
    {
      id: 8,
      name: 'Sotanghon Soup with Alimango',
      description: 'yyy jduebne hseeke',
      price: 6
    },
    {
      id: 9,
      name: 'Ginisa na Pampano sa Tausi',
      description: 'ddddd udyeje jyey8kslsh',
      price: 12
    },
    {
      id: 10,
      name: 'Beef Caldereta',
      description: 'aaaa jdjue jdkkei',
      price: 2
    }
  ])

  cart = [];
  cartCount = new BehaviorSubject(0);
  cartAmount = new BehaviorSubject(0);

  constructor() { }

  addCart(item) {
    if(item.count == 0) {
      this.cart.push(item)
    }
    ++item.count;
    this.cartCount.next(this.cartCount.value + 1)
    this.cartAmountCalculation()
  }

  removeCart(item) {
    --item.count;
    if(item.count == 0) {
      this.cart.splice(this.cart.findIndex(x => x.id == item.id), 1);
    }
    this.cartCount.next(this.cartCount.value - 1)
    this.cartAmountCalculation()
  }

  cartAmountCalculation() {
    this.cartAmount.next(this.cart.reduce((i,j) => i + (j.count * j.price), 0));
  }

}
